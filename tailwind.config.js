module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      screens:{
        'print': {'raw': 'print'}
      },
      width: {
        "27": "27%",
        "73": "73%",
        "px-32": "32px"
      },
      height: {
        "15": "15%",
        "85": "85%",
        "px-32": "32px",
        "px-168": "168px",
        "px-954": "954px"
      },
      flexGrow: {
        '0':0,
        '1':1,
        '16':16
      },
      spacing: {
        'px-5':'5px',
        'px-10': '10px'
      },
      padding: {
        "15": "15px"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
